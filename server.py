import asyncio
import logging
from typing import AsyncIterable
import secrets
import inspect

import streaming_pb2
import streaming_pb2_grpc

import grpc
import google.protobuf.timestamp_pb2


class StreamingServicer(streaming_pb2_grpc.StreamingServicer):

    def __init__(self) -> None:
        logging.info("init")

    async def Echo(self, request_iterator: AsyncIterable[streaming_pb2.EchoRequest], context) -> AsyncIterable[streaming_pb2.EchoResponse]:
        async for request in request_iterator:
            now = google.protobuf.timestamp_pb2.Timestamp()
            now.GetCurrentTime()
            logging.info(f"Request with text {request.text} at timestamp {now.ToDatetime()}")
            yield streaming_pb2.EchoResponse(text=request.text, processed_at=now)

    async def Data(self, request_iterator: AsyncIterable[streaming_pb2.DataRequest], context) -> AsyncIterable[streaming_pb2.DataResponse]:
        async for request in request_iterator:
            now = google.protobuf.timestamp_pb2.Timestamp()
            now.GetCurrentTime()
#            data = secrets.token_bytes(request.response_bytes)
            data = b'This is some data which is repeated and can be compressed well..' * int(request.response_bytes/64)
            logging.info(f"DataRequest from {context.peer()} with id {request.message_id}. Sending {len(data)} bytes with compression={request.enable_response_compression}")
            if not request.enable_response_compression:
                context.disable_next_message_compression()
            yield streaming_pb2.DataResponse(message_id=request.message_id, requested_at=request.requested_at, responded_at=now, response_data=data)


async def serve() -> None:
    server = grpc.aio.server(compression=grpc.Compression.Gzip)
    streaming_pb2_grpc.add_StreamingServicer_to_server(
        StreamingServicer(), server)
    server.add_insecure_port('[::]:50051')
    await server.start()
    await server.wait_for_termination()


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    loop = asyncio.new_event_loop()
    loop.run_until_complete(serve())
