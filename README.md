Simple gRPC bi-directional streaming example based on Python gRPC AsyncIO.

To run locally (needs Python 3.10+ installed)
```
# Create virtual environment
python -m venv venv
. ./venv/bin/activate
pip install -r requirements.txt

# runs a server on port 50051
python server.py 

# client 
python client.py localhost:50051
```

To run with Docker
```
docker build . -t python-grpc-streaming

# run server
docker run --rm -i -t -p 50051:50051 python-grpc-streaming 

# run client (replace address and port accordingly)
docker run --rm -i -t python-grpc-streaming python client.py <address>:<port>
```

To run the CI-generated image:
```
docker run --rm -i -t -p 50051:50051 registry.gitlab.com/cbenien/python-grpc-streaming
```




