import asyncio
import logging
from typing import AsyncIterable
import sys
import grpc

import google.protobuf.timestamp_pb2
import streaming_pb2
import streaming_pb2_grpc



async def send_echo_requests(count: int) -> AsyncIterable[streaming_pb2.EchoRequest]:
    for i in range(0, count):
        await asyncio.sleep(1)
        yield streaming_pb2.EchoRequest(text=f"Message {i}")


def get_grpc_channel(grpc_url):
    if grpc_url.endswith(":443"):
        logging.info(f"Creating secure channel to {grpc_url}")
        return grpc.aio.secure_channel(grpc_url, grpc.ssl_channel_credentials())
    logging.info(f"Creating insecure channel to {grpc_url}")
    return grpc.aio.insecure_channel(grpc_url)


async def run(grpc_url) -> None:

    async with get_grpc_channel(grpc_url) as channel:
        stub = streaming_pb2_grpc.StreamingStub(channel)
        call = stub.Echo(send_echo_requests(3))
        async for response in call:
            logging.info(f"Received response {response.text} at timestamp {response.processed_at.ToDatetime()}")


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    grpc_url = sys.argv[1]
    loop = asyncio.new_event_loop()
    loop.run_until_complete(run(grpc_url))
