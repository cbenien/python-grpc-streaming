FROM python:3

WORKDIR /app/
COPY requirements.txt /app/
RUN pip install -r requirements.txt
COPY *.py /app/

USER 1500:1500
EXPOSE 50051
CMD ["python", "server.py"]
