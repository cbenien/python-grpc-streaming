import argparse
import asyncio
import logging
import statistics
from typing import AsyncIterable
import sys
import grpc

import google.protobuf.timestamp_pb2
import streaming_pb2
import streaming_pb2_grpc
import datetime


async def send_data_requests(count: int, sleep: float, data_size: int, response_compression: bool) -> AsyncIterable[streaming_pb2.DataRequest]:
    for i in range(0, count):
        await asyncio.sleep(sleep)
        now = google.protobuf.timestamp_pb2.Timestamp()
        now.GetCurrentTime()
        request_data = b'\0' * 100
        response_bytes = 0
        if i == count-1:
            response_bytes = data_size
        yield streaming_pb2.DataRequest(message_id=f"Message {i}", requested_at=now, request_data=request_data,
                                        response_bytes=response_bytes, enable_response_compression=response_compression)


def get_grpc_channel(grpc_url: str, no_tls: bool):
    if not no_tls:
        logging.info(f"Creating secure channel to {grpc_url}")
        return grpc.aio.secure_channel(grpc_url, grpc.ssl_channel_credentials())
    logging.info(f"Creating insecure channel to {grpc_url}")
    return grpc.aio.insecure_channel(grpc_url)


async def run(grpc_url) -> None:
    async with get_grpc_channel(args.server, args.no_tls) as channel:
        stub = streaming_pb2_grpc.StreamingStub(channel)
        call = stub.Data(send_data_requests(args.count, args.delay, args.bytes, args.compression))
        clock_skew = []
        async for response in call:

            now = datetime.datetime.now()
            requested_at = response.requested_at.ToDatetime()
            responded_at = response.responded_at.ToDatetime()

            total_time = (now-requested_at).total_seconds() * 1000

            # Assume request and response take same amount of time (network roundtrip time)
            cs = (now - (now - requested_at) / 2) - responded_at
            clock_skew.append(cs.total_seconds() * 1000)

            ss = sorted(clock_skew)
            if len(ss) > 1:
                ss = ss[1:]
            if len(ss) > 1:
                ss = ss[0:len(ss)-1]
            if len(ss) > 1:
                ss = ss[1:]
            if len(ss) > 1:
                ss = ss[0:len(ss)-1]

            avg_clock_skew = statistics.mean(clock_skew)

            request_time = (responded_at + datetime.timedelta(milliseconds=avg_clock_skew) - requested_at).total_seconds() * 1000
            response_time = (now - datetime.timedelta(milliseconds=avg_clock_skew) - responded_at).total_seconds() * 1000

            logging.info(f"Received response {response.message_id}: length={len(response.response_data)} total_time={total_time:.2f}ms request_time={request_time:.2f} response_time={response_time:.2f}ms clock_skew={avg_clock_skew:.2f}ms")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='gRPC network test client.')
    parser.add_argument('--count', type=int, default=10,
                        help='Number of request messages')
    parser.add_argument('--delay', type=float, default=0.1,
                        help='Seconds to sleep between requests')
    parser.add_argument('--bytes', type=int, default=32768,
                        help='Response bytes')
    parser.add_argument('--no-tls', action='store_true',
                        help='Disable TLS, create insecure channel')
    parser.add_argument('--compression', action='store_true',
                        help='Tells server to compress response messages')
    parser.add_argument('server',
                        help='Server endpoint in the form host:port')

    args = parser.parse_args()

    logging.basicConfig(level=logging.INFO)
    loop = asyncio.new_event_loop()
    loop.run_until_complete(run(args))

# a5386c7b425e64f428789e258acbf3d0-188e569229da9cce.elb.us-east-1.amazonaws.com:50051

